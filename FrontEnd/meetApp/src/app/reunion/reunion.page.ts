import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AjoutereunionModalComponent } from '../ajoutereunion-modal/ajoutereunion-modal.component';
import * as types from '../interfaces/indx';

@Component({
  selector: 'app-reunion',
  templateUrl: './reunion.page.html',
  styleUrls: ['./reunion.page.scss'],
})
export class ReunionPage implements OnInit { 
  public endPoints = "/reunions/";
  public dataFutur;
  public longFutur=-1;
  public dataArchive;
  public longArchive=-1;
  public dataEncours;
  public longEncours=-1;
  public dataGlobal;

  constructor(private httpClient: HttpClient, private route: Router, private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  afficheReunionFutur(){

    this.httpClient.get("http://localhost:1337"+this.endPoints).subscribe(data => {

      this.dataGlobal = data;
       this.longFutur = this.dataGlobal.length;
       this.longArchive=-1;
       this.longEncours=-1;
          for (let index = 0; index < this.dataGlobal.length; index++) {
            
            let element = this.dataGlobal[index];
            
              if (element.dateFin > new Date()&&element.dateDebut > new Date) {
                
                this.dataFutur.push(element);
            }
      }

      },error => {
  
        console.log(error);
        return null;
      });

  }

  afficheReunionEncours(){

    this.httpClient.get("http://localhost:1337"+this.endPoints).subscribe(data => {

      this.dataGlobal = data;
       this.longEncours = this.dataGlobal.length;
       this.longArchive=-1;
       this.longFutur=-1;
          for (let index = 0; index < this.dataGlobal.length; index++) {
            
            let element = this.dataGlobal[index];
            
              if (element.dateFin > new Date()&&element.dateDebut < new Date) {
                
                this.dataEncours.push(element);
            }
      }

      },error => {
  
        console.log(error);
        return null;
      });

  }

  afficheReunionArchive(){

    this.httpClient.get("http://localhost:1337"+this.endPoints).subscribe(data => {

      this.dataGlobal = data;
       this.longArchive = this.dataGlobal.length;
       this.longFutur=-1;
       this.longEncours=-1;
      for (let index = 0; index < this.dataGlobal.length; index++) {
        let element = this.dataGlobal[index];
           if (element.dateFin < new Date()) {
             this.dataArchive.push(element);        
        }
 }

      },error => {
  
        console.log(error);
        return null;
      });
  
}

  dataReunion(){
    
    this.httpClient.get("http://localhost:1337"+this.endPoints).subscribe(data => {

      this.dataGlobal = data;
      },error => {
  
        console.log(error);
        return null;
      });
  }

  async modalReunion(reunion){
    const modal = await this.modalCtrl.create({
      component: AjoutereunionModalComponent
    });

    await modal.present();

  }

}
