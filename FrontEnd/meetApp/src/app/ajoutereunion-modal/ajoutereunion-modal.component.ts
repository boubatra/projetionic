import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-ajoutereunion-modal',
  templateUrl: './ajoutereunion-modal.component.html',
  styleUrls: ['./ajoutereunion-modal.component.scss'],
})
export class AjoutereunionModalComponent implements OnInit {

  public balanceInput = new FormControl('',Validators.required);
  public cod;
  public salles;
  public sal;
  public postId;
  public dateD;
  public dateF;
  public valid;
  public dataReunion;
  public endPoints = "/reunions/";

  constructor(private modalCtrl: ModalController, private httpClient: HttpClient, private router: Router, public navCtrl: NavController) { }

  ngOnInit() {}

  dismissModal() {
    this.modalCtrl.dismiss();
}


newCode(ev)
{

  this.cod= ev.detail.value;
  
  if (ev.detail.value!=null) {
    this.valid=true;  
  }
  if(ev.detail.value===""){
    this.valid=false; 
  }
  
}

newDate1(ev)
{
  this.dateD= ev.detail.value;

  if (ev.detail.value!=null && this.valid) {
    this.valid=true;
     
  }
  if(ev.detail.value===""){
    this.valid=false;
     
  }

}

newDate2(ev)
{
  this.dateF= ev.detail.value;

  if (ev.detail.value!=null && this.valid) {
    this.valid=true;
      
  }
  if(ev.detail.value===""){
    this.valid=false;
    
  }

}

newSal(ev)
{
  this.sal= ev.detail.value;

  if (ev.detail.value!=null && this.valid) {
    
    this.balanceInput.setValue(false);  
  }
  if(ev.detail.value===""){
    
    this.balanceInput = new FormControl('',Validators.required); 
  }

}

addDirection(){

  this.httpClient.post<any>('http://localhost:1337'+this.endPoints, {code: this.cod, dateDebut: this.dateD, dateFin: this.dateF, salle: this.sal }).subscribe(data => {
      
  this.postId = data.id;

  this.router.navigateByUrl('/menu/reunion');
      
      this.httpClient.get("http://localhost:1337/reunions").subscribe(data1 => {

          this.dataReunion=data1;

          this.dismissModal();

          this.router.navigateByUrl('/menu/reunion');

          },error => {

            console.log(error);
            
          });
  },error => {

    console.log(error);
    
  });
  
}

}
